FROM golang:1.10.0-alpine
COPY . /go/src/gitlab.com/rochacon/ifconfigme
RUN go install -v gitlab.com/rochacon/ifconfigme
ENV PORT=80
EXPOSE 80
CMD ["/go/bin/ifconfigme"]
